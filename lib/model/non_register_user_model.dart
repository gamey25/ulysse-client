 import 'package:ulysse_all_front/model/user_model.dart';
import 'package:ulysse_all_front/screen/authentification/login.dart';

class NonRegisterUserModel extends UserModel{

  String password="";
  NonRegisterUserModel(int id, String firstName, String secondName, String phoneNumber, String poste, String email, this.password) 
                      : super(id, firstName, secondName, phoneNumber, poste, email);

  factory NonRegisterUserModel.fromJson(Map<String, dynamic> i)=>NonRegisterUserModel(
    i['id'],
    i['first_name'],
    i['second_name'],
    i['phone_number'],
    i['poste'],
    i['email'],
    i['password']
  );

  @override
  Map<String, dynamic> toMap()=>{
    "first_name":firstName,
    "second_name":secondName,
    "phone_number":phoneNumber,
    "poste":poste,
    "email":email,
    "password":password
  }; 

  

}