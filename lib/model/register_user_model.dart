import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:ulysse_all_front/model/user_model.dart';

class RegisterUserModel extends UserModel {
  RegisterUserModel(int id, String firstName, String secondName,
      String phoneNumber, String poste, String email)
      : super(id, firstName, secondName, phoneNumber, poste, email);

  factory RegisterUserModel.fromJson(Map<String, dynamic> i) =>
      RegisterUserModel(
        i['id'],
        i['first_name'],
        i['second_name'],
        i['phone_number'],
        i['poste'],
        i['email'],
      );

  @override
  Map<String, dynamic> toMap() => {
        "id": id,
        "first_name": firstName,
        "second_name": secondName,
        "phone_number": phoneNumber,
        "poste": poste,
        "email": email
      };

  static Future<Map<String, dynamic>> getDataUser() async {
    Map<String, dynamic> jsonUser;
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    var data = localStorage.getString('user');
    
    if (data != "nothing") {
      if (localStorage.getString('user') != null) {
        String? userData = localStorage.getString('user');
        jsonUser = {"success": true, "data": jsonDecode(userData!)};
      } else {
        jsonUser = {"success": false};
      }
    } else {
      print("data print $data");
      jsonUser = {"success": false};
    }
    return jsonUser;
  }

  static Future<bool> logOutUser() async {
    bool jsonUser = false;
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    if (localStorage.containsKey('user')) {
      final success1 = await localStorage.setString("user", "nothing");
      final success2 = await localStorage.setString("token", "nothing");
      //final success3 = await localStorage.clear();
      //await localStorage.reload();
      localStorage.commit();
      if (success1 == true && success2 == true) {
        jsonUser = true;
      } else {
        jsonUser = false;
      }
    } else {
      jsonUser = false;
    }
    print(jsonUser);
    return jsonUser;
  }

  Future<String> saveTokenAfterRegistering(Map<String, dynamic> data) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('token', jsonEncode(data['token']['token']));
    localStorage.setString('user', jsonEncode(data['user']));
    localStorage.commit();
    //print('success storage token');
    return 'success storage token';
  }

  static Future<String> login(Map<String, dynamic> data) async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    localStorage.setString('token', jsonEncode(data['token']['token']));
    localStorage.setString('user', jsonEncode(data['user']));
    localStorage.commit();
    //print('success storage token');
    return 'success storage token';
  }
}
