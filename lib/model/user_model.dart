class UserModel{
  
  int id=-1;
  String firstName="";
  String secondName="";
  String phoneNumber="";
  String email="";
  String poste="";
  UserModel(this.id, this.firstName, this.secondName, this.phoneNumber, this.poste, this.email);
  
  factory UserModel.fromJson(Map<String, dynamic> i)=>UserModel(
    i['id'],
    i['first_name'],
    i['second_name'],
    i['phone_number'],
    i['poste'],
    i['email']
  );

  Map<String, dynamic> toMap()=>{
    "id":id,
    "first_name":firstName,
    "second_name":secondName,
    "phone_number":phoneNumber,
    "poste":poste,
    "email":email
  }; 

  
} 