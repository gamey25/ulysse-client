import 'package:flutter/material.dart';
import 'package:ulysse_all_front/route_names/route_names.dart';
import 'package:ulysse_all_front/screen/homePage.dart';

void main(List<String> args) {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      //color: Colors.redAccent.withOpacity(0.8),
      debugShowCheckedModeBanner: false,
      home: HomePage(),
      routes: RouteName.getRouteNameBoss(),
    );
  }
}
