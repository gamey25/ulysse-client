import 'package:flutter/cupertino.dart';
import 'package:ulysse_all_front/screen/boss_screen/accueil.dart';
import 'package:ulysse_all_front/screen/boss_screen/accueil_hotel.dart';
import 'package:ulysse_all_front/screen/client_screen/accueil.dart';
import 'package:ulysse_all_front/screen/homePage.dart';

class RouteName {
  RouteName();
  static Map<String, WidgetBuilder> getRouteNameBoss() {
    return <String, WidgetBuilder>{
      //'/accueil-boss': (BuildContext context) => AcceuilBoss(),
      '/accueil-client': (BuildContext context) => AccueilClient(),
      '/home-page': (BuildContext context) => HomePage(),
      "/mes-hotels":(BuildContext context) => HomePage(),
      //"/accueil-hotels":(BuildContext context) => AcceuilHotel(login),
      "/mes-restaurants":(BuildContext context) => HomePage(),
      "/mes-transports":(BuildContext context) => HomePage(),
      "/mes-loisirs":(BuildContext context) => HomePage(),
      "/mes-espaces-tourisme":(BuildContext context) => HomePage(),
    };
  }
}
