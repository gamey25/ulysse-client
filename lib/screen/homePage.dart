import 'package:flutter/material.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/screen/authentification/login.dart';
import 'package:ulysse_all_front/screen/authentification/register.dart';
import 'package:ulysse_all_front/screen/boss_screen/accueil.dart';
import 'package:ulysse_all_front/screen/client_screen/accueil.dart';

class HomePage extends StatefulWidget {
  //const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool visible = true, login = false, zoneClient = true;
  String poste = "";

  isConnected() {
    RegisterUserModel.getDataUser().then((value) {
      if (value["success"] == true) {
        setState(() {
          print("j'ai pris le poste");
          print(value);
          poste = value["data"]["poste"];

          login = true;
        });
      } else {
        setState(() {
          login = false;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    isConnected();
  }

  chooseAccueilZone(String nameZone) {
    setState(() {
      login = true;
      if (nameZone == "accueuil-boss") {
        poste = "Proprietaire";
        zoneClient = true;
      } else {
        zoneClient = false;
      }
    });
  }

  toggle() {
    setState(() {
      visible = !visible;
    });
  }

  deconnected() {
    setState(() {
      login = false;
      visible = true;
      print("j'ai clique login=$login, visible=$visible");
    });
  }

  @override
  Widget build(BuildContext context) {
    return login
        ? poste == "Proprietaire"
            ? AcceuilBoss(deconnected)
            : AccueilClient()
        : visible
            ? Login(toggle, chooseAccueilZone)
            : Register(toggle);
  }
}
