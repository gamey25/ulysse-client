import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/screen/boss_screen/accueil_hotel.dart';
import 'package:ulysse_all_front/screen/homePage.dart';
import 'package:ulysse_all_front/service/user_service.dart';
import 'package:ulysse_all_front/widgets/customSnackBar.dart';
import 'package:ulysse_all_front/widgets/custom_card_drawer.dart';
import 'package:ulysse_all_front/widgets/custom_department_presentation.dart';

class AcceuilBoss extends StatefulWidget {
  final VoidCallback login;
  const AcceuilBoss(this.login, {super.key});

  @override
  State<AcceuilBoss> createState() => _AcceuilBossState();
}

class _AcceuilBossState extends State<AcceuilBoss> {
  String name = "";
  String prenom = "";
  String phone = "";

  _AcceuilBossState() {
    getFirstNameUser();
  }

  void getFirstNameUser() {
    //String name = " ";
    RegisterUserModel.getDataUser().then((value) {
      setState(() {
        print(value);
        if (value["success"] == true) {
          name = value["data"]["first_name"];
          prenom = value["data"]["second_name"];
          phone = value["data"]["phone_number"];
        } else {
          name = "Nom ";
          name = "Prenom";
          phone = "000-000-000";
        }
      });
    });

    //return name;
  }

  @override
  Widget build(BuildContext context) {
    deconnected() {
      setState(() {
        widget.login.call();
      });
    }

    MaterialPageRoute routeHomePage =
        MaterialPageRoute(builder: (BuildContext context) => HomePage());

    MediaQueryData mq = MediaQuery.of(context);

    return Scaffold(
      //backgroundColor: Colors.redAccent.withOpacity(0.5),
      drawerEnableOpenDragGesture: true,
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Row(
                children: [
                  Icon(Icons.person, color: Colors.white, size: 100),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          flex: 1,
                          child: SizedBox(
                            height: 10,
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.prenom,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.phone,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(flex: 1, child: Text("Gestionnaire ULYSSE")),
                    ],
                  )
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.redAccent[100],
              ),
            ),
            CustomCardDrawer.getTheCard(context, Icons.home_outlined, "Accueil",
                "cliquez pour retourner a l'accueil", () {
              print("j'ai clique accueil boss ");
              Navigator.pushNamed(context, "/home-page");
            }),
            CustomCardDrawer.getTheCard(context, Icons.hotel_rounded,
                "Mes Hotels", "cliquez pour gerer vos hotels", () {
              print("j'ai clique hotel ");
              MaterialPageRoute routeHotel = MaterialPageRoute(
                  builder: (BuildContext context) => AcceuilHotel(deconnected));
              Navigator.of(context).push(routeHotel);
            }),
            CustomCardDrawer.getTheCard(context, Icons.restaurant_rounded,
                "Mes Restaurants", "cliquez pour gerer vos restaurants", () {
              print("j'ai clique restaurant ");
            }),
            CustomCardDrawer.getTheCard(context, Icons.car_rental,
                "Mes Transports", "cliquez pour gerer vos transports", () {
              print("j'ai clique transport ");
            }),
            CustomCardDrawer.getTheCard(
                context,
                Icons.add_reaction_rounded,
                "Mes Espaces loisirs",
                "cliquez pour gerer vos espaces de loisirs", () {
              print("j'ai clique loisirs");
            }),
            CustomCardDrawer.getTheCard(
                context,
                Icons.public,
                "Mes Lieux de tourisme",
                "cliquez pour gerer vos espaces de tourisme", () {
              print("j'ai clique tourisme ");
            }),
          ],
        ),
      ),
      /* appBar: AppBar(
        backgroundColor: Colors.redAccent,
        title: Text('Accueil'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.logout_outlined,
            ),
            hoverColor: Colors.amber,
            tooltip: 'Se deconnecter',
            onPressed: () {
              UserService.logout().then((value) {
                widget.login.call();
                print("test logout");
                print(value);
                //Navigator.pushNamed(context, '/home-page');
              });
            },
          ),
        ],
      ), */
      body: CustomScrollView(
        physics: BouncingScrollPhysics(),
        slivers: [
          SliverAppBar(
            title: Text(
              "Accueil",
              style: TextStyle(
                color: Colors.redAccent.withOpacity(0.9),
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            centerTitle: true,
            pinned: true,
            floating: false,
            expandedHeight: mq.size.height / 5,
            backgroundColor: Colors.redAccent.withOpacity(0.9),
            foregroundColor: Colors.redAccent.withOpacity(0.9),
            flexibleSpace: Carousel(
              images: [
                AssetImage("assets/images/c1.jpg"),
                AssetImage("assets/voitures/v1.jpg"),
                AssetImage("assets/restaurants/r1.jpg"),
                AssetImage("assets/images/c5.jpg"),
                AssetImage("assets/images/c2.jpg"),
                AssetImage("assets/voitures/v2.jpg"),
                AssetImage("assets/restaurants/r2.jpg"),
                AssetImage("assets/images/c9.jpg"),
                AssetImage("assets/images/c3.jpg"),
                AssetImage("assets/voitures/v3.jpg"),
                AssetImage("assets/restaurants/r5.jpg"),
                AssetImage("assets/images/c7.jpg"),
                AssetImage("assets/images/c4.jpg"),
                AssetImage("assets/voitures/v4.jpg"),
                AssetImage("assets/restaurants/r4.jpg"),
                AssetImage("assets/images/c8.jpg"),
              ],
              dotSize: 8.0,
              dotSpacing: 15.0,
              dotColor: Colors.red,
              indicatorBgPadding: 4.0,
              dotBgColor: Colors.redAccent.withOpacity(0.3),
              dotIncreasedColor: Colors.redAccent.withOpacity(0.9),
              //dotPosition: DotPosition.topCenter,
              //borderRadius: true,
            ),
            //),
            actions: <Widget>[
              IconButton(
                icon: const Icon(
                  Icons.logout_outlined,
                ),
                hoverColor: Colors.blueAccent,
                tooltip: 'Se deconnecter',
                onPressed: () {
                  UserService.logout().then((value) {
                    widget.login.call();
                    print("test logout");
                    print(value);
                    //Navigator.pushNamed(context, '/home-page');
                  });
                },
              ),
            ],
          ),
          SliverFixedExtentList(
            itemExtent: 150.0,
            delegate: SliverChildListDelegate(
              [
                CustomDepartmentPresentation.getTheCustomDepartmentPresentation(
                    numStar: 3,
                    urlImage: "assets/images/c7.jpg",
                    //context: context,
                    onClick: () {
                      print("j'ai clique custodeparpresen 1 ");
                      MaterialPageRoute routeHotel = MaterialPageRoute(
                          builder: (BuildContext context) =>
                              AcceuilHotel(deconnected));
                      Navigator.of(context).push(routeHotel);
                    }),
                CustomDepartmentPresentation.getTheCustomDepartmentPresentation(
                    numStar: 2,
                    zone: "de restaurants",
                    urlImage: "assets/restaurants/r4.jpg",
                    //context: context,
                    onClick: () {
                      print("j'ai clique custodeparpresen 2 ");
                    }),
                CustomDepartmentPresentation.getTheCustomDepartmentPresentation(
                    numStar: 4,
                    zone: "de vehicules",
                    urlImage: "assets/voitures/v4.jpg",
                    //context: context,
                    onClick: () {
                      print("j'ai clique custodeparpresen 3 ");
                    }),
                CustomDepartmentPresentation.getTheCustomDepartmentPresentation(
                    numStar: 1,
                    zone: "de loisirs",
                    urlImage: "assets/restaurants/r1.jpg",
                    //context: context,
                    onClick: () {
                      print("j'ai clique custodeparpresen 4 ");
                    }),
                CustomDepartmentPresentation.getTheCustomDepartmentPresentation(
                    numStar: 5,
                    zone: "d'espace tourisme",
                    urlImage: "assets/restaurants/r2.jpg",
                    //context: context,
                    onClick: () {
                      print("j'ai clique custodeparpresen 5 ");
                    }),
              ],
            ),
          )
        ],
      ),
    );
  }
}
