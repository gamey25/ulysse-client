import 'dart:io';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/screen/homePage.dart';
import 'package:ulysse_all_front/service/user_service.dart';
import 'package:ulysse_all_front/widgets/customTextField.dart';
import 'package:ulysse_all_front/widgets/custom_card_drawer.dart';
import 'package:ulysse_all_front/widgets/custom_drop_down_list.dart';
import 'package:ulysse_all_front/widgets/custom_form_hostel.dart';
import 'package:ulysse_all_front/widgets/custom_input_decoration.dart';
import 'package:image_pickers/image_pickers.dart';

class AcceuilHotel extends StatefulWidget {
  final VoidCallback login;
  const AcceuilHotel(this.login, {super.key});

  @override
  State<AcceuilHotel> createState() => _AcceuilHotelState();
}

class _AcceuilHotelState extends State<AcceuilHotel> {
  String name = "";
  String prenom = "";
  String phone = "";

  List<String> listItem = [
    "Chambre simple",
    "Chambre moderne",
    "Appartement simple",
    "Appartement meuble",
    "Studio simple",
    "Studio moderne",
    "Studio meuble"
  ];
  List<String> listCountry = [
    "Cameroun",
    "Cote d'ivoire",
    "Nigeria",
  ];

  List<String> listCity1 = [
    "Yaounde",
    "Douala",
    "Bafoussam",
  ];
  List<String> listCity2 = [
    "P2C1",
    "P2C2",
    "P2C3",
    "P2C4",
    "P2C5",
  ];
  List<String> listCity3 = [
    "P3C1",
    "P3C2",
  ];
  List<String> listCity = [""];
  List<String> listQuartier = [""];
  List<String> listQuartier1 = [
    "Mboppi",
    "Mvog-ada",
    "Effoulan",
  ];
  List<String> listQuartier2 = [
    "C1Q15",
    "C2Q15",
    "C3Q15",
    "C4Q15",
    "C5Q15",
  ];
  List<String> listQuartier3 = [
    "C3Q1",
    "C3Q2",
  ];

  List<String> listValabilite = [
    "Journee",
    "Nuitee",
    "Semaine",
    "Weekend",
    "Mois",
    "Annee",
  ];

  CustomDropDownList typeLogement = CustomDropDownList(
    listItem: [""],
    onChange: ((changeValue) {
      print("la nouvelle value est: $changeValue");
    }),
    err: "Veuillez selectionner un type de logement",
    label: "Type logement",
    hintText: "Type logement",
  );
  CustomTextField description = CustomTextField(
      title: "Description de l'hebergement",
      placeholder: "Enter une description",
      numLine: 4,
      err: "Enter une description");

  CustomDropDownList quartier = CustomDropDownList(
    listItem: [""],
    onChange: ((changeValue) {
      print("la nouvelle quartier est: $changeValue");
    }),
    err: "Veuillez selectionner une quartier",
    label: "Quartier",
    hintText: "Quartier",
  );

  CustomDropDownList ville = CustomDropDownList(
    listItem: [""],
    onChange: ((changeValue) {
      print("la nouvelle ville est: $changeValue");
    }),
    err: "Veuillez selectionner une ville",
    label: "Ville",
    hintText: "Ville",
  );
  Widget ddVille = Text("");
  Widget ddQuartier = Text("");

  CustomDropDownList pays = CustomDropDownList(
    listItem: [""],
    onChange: ((changeValue) {
      print("le nouveau pays est: $changeValue");
    }),
    err: "Veuillez selectionner un pays",
    label: "Pays",
    hintText: "Pays",
  );

  CustomDropDownList dropDownValabite = CustomDropDownList(
    listItem: [""],
    onChange: ((changeValue) {
      print("la nouvelle valabilite est: $changeValue");
    }),
    err: "Veuillez selectionner une valabilite",
    label: "Valabilite",
    hintText: "Valabilite",
  );

  CustomTextField montantTarif = CustomTextField(
      title: "Montant du tarif",
      placeholder: "Montant du tarif",
      numLine: 1,
      err: "Enter une description");

  CustomTextField descriptionTarif = CustomTextField(
      title: "Description du tarif",
      placeholder: "Detail tarif",
      numLine: 4,
      err: "Enter une description");

  void callbackPays(String? changeValue) {
    if (changeValue!.compareTo("Cameroun") == 0) {
      //ville.listItem = listCity1;
      ville = CustomDropDownList(
        listItem: listCity1,
        onChange: callbackVille,
        err: "Veuillez selectionner une ville",
        label: "Ville",
        hintText: "Ville",
      );
      //ddVille = ville.getDropDownListItem();
      print("je suis la 1");
    }
    if (changeValue.compareTo("Cote d'ivoire") == 0) {
      //ville.listItem = listCity2;
      ville = CustomDropDownList(
        listItem: listCity2,
        onChange: callbackVille,
        err: "Veuillez selectionner une ville",
        label: "Ville",
        hintText: "Ville",
      );
      //ddVille = ville.getDropDownListItem();
      print("je suis la 2");
    }
    if (changeValue.compareTo("Nigeria") == 0) {
      //ville.listItem = listCity3;
      ville = CustomDropDownList(
        listItem: listCity3,
        onChange: callbackVille,
        err: "Veuillez selectionner une ville",
        label: "Ville",
        hintText: "Ville",
      );
      //ddVille = ville.getDropDownListItem();
      print("je suis la 3");
    }
    setState(() {
      ddVille = ville.getDropDownListItem();

      print("je suis a ddville");
    });
    print("le nouveau pays est: $changeValue");
  }

  void callbackVille(String? changeValue) {
    if (changeValue!.compareTo("Yaounde") == 0) {
      quartier = CustomDropDownList(
        listItem: listQuartier1,
        onChange: callbackQuartier,
        err: "Veuillez selectionner un quartier ",
        label: "Quartier",
        hintText: "Quartier",
      );
      print("je suis la a");
    }
    if (changeValue.compareTo("Douala") == 0) {
      quartier = CustomDropDownList(
        listItem: listQuartier2,
        onChange: callbackQuartier,
        err: "Veuillez selectionner un quartier ",
        label: "Quartier",
        hintText: "Quartier",
      );
      print("je suis la b");
    }
    if (changeValue.compareTo("Bafoussam") == 0) {
      quartier = CustomDropDownList(
        listItem: listQuartier3,
        onChange: callbackQuartier,
        err: "Veuillez selectionner un quartier ",
        label: "Quartier",
        hintText: "Quartier",
      );
      print("je suis la c");
    }
    setState(() {
      ddQuartier = quartier.getDropDownListItem();
      print("je suis a ddquartier");
    });

    print("la nouvelle ville est: $changeValue");
  }

  void callbackValabilite(String? changeValue) {
    setState(() {
      dropDownValabite.val = changeValue!;
    });
  }

  void callbackLogement(String? changeValue) {
    setState(() {
      typeLogement.val = changeValue!;
    });
  }

  void callbackQuartier(String? changeValue) {
    setState(() {
      quartier.val = changeValue!;
    });
  }

  _AcceuilHotelState() {
    listItem = [
      "Chambre simple",
      "Chambre moderne",
      "Appartement simple",
      "Appartement meuble",
      "Studio simple",
      "Studio moderne",
      "Studio meuble"
    ];
    typeLogement = CustomDropDownList(
      listItem: listItem,
      onChange: callbackLogement,
      err: "Veuillez selectionner un type de logement",
      label: "Type logement",
      hintText: "Type logement",
    );

    quartier = CustomDropDownList(
      listItem: listQuartier1,
      onChange: callbackQuartier,
      err: "Veuillez selectionner un quartier ",
      label: "Quartier",
      hintText: "Quartier",
    );

    ddQuartier = quartier.getDropDownListItem();

    ville = CustomDropDownList(
      listItem: listCity1,
      onChange: callbackVille,
      err: "Veuillez selectionner une ville",
      label: "Ville",
      hintText: "Ville",
    );

    dropDownValabite = CustomDropDownList(
      listItem: listValabilite,
      onChange: callbackValabilite,
      err: "Veuillez selectionner une valabilite",
      label: "Valabilite",
      hintText: "Valabilite",
    );

    ddVille = ville.getDropDownListItem();
    pays = CustomDropDownList(
      listItem: listCountry,
      onChange: callbackPays,
      err: "Veuillez selectionner un pays",
      label: "Pays",
      hintText: "Pays",
    );

    getFirstNameUser();
  }

  void getFirstNameUser() {
    //String name = " ";
    RegisterUserModel.getDataUser().then((value) {
      setState(() {
        print(value);
        if (value["success"] == true) {
          name = value["data"]["first_name"];
          prenom = value["data"]["second_name"];
          phone = value["data"]["phone_number"];
        } else {
          name = "Nom ";
          name = "Prenom";
          phone = "000-000-000";
        }
      });
    });

    //return name;
  }

  GalleryMode _galleryMode = GalleryMode.image;
  GlobalKey? globalKey;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    globalKey = GlobalKey();
  }

  List<Media> _listImagePaths = [];
  String? dataImagePath = "";
  Future<void> selectImages() async {
    try {
      _galleryMode = GalleryMode.image;
      _listImagePaths = await ImagePickers.pickerPaths(
        galleryMode: _galleryMode,
        showGif: true,
        selectCount: 4,
        showCamera: true,
        cropConfig: CropConfig(enableCrop: true, height: 1, width: 1),
        compressSize: 1500,
        uiConfig: UIConfig(uiThemeColor: Colors.redAccent),
      );
      print("nombre d'images choisies: $_listImagePaths.length");
      if (_listImagePaths.length > 0) {
        _listImagePaths.forEach((media) {
          print("emplacement du fichier: ${media.path.toString()}");
        });
      }
      setState(() {});
    } on PlatformException {}
  }

  @override
  Widget build(BuildContext context) {
    MaterialPageRoute routeHomePage =
        MaterialPageRoute(builder: (BuildContext context) => HomePage());
    MaterialPageRoute routeHotel = MaterialPageRoute(
        builder: (BuildContext context) => AcceuilHotel(widget.login));
    MediaQueryData mq = MediaQuery.of(context);

    return Scaffold(
      //backgroundColor: Colors.redAccent.withOpacity(0.5),
      /*drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              child: Row(
                children: [
                  Icon(Icons.person, color: Colors.white, size: 100),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                          flex: 1,
                          child: SizedBox(
                            height: 10,
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.name,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.prenom,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(
                          flex: 1,
                          child: Text(
                            this.phone,
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          )),
                      Expanded(flex: 1, child: Text("Gestionnaire ULYSSE")),
                    ],
                  )
                ],
              ),
              decoration: BoxDecoration(
                color: Colors.redAccent[100],
              ),
            ),
            CustomCardDrawer.getTheCard(context, Icons.home_outlined, "Accueil",
                "cliquez pour retourner a l'accueil", () {
              print("j'ai clique accueil boss ");
              Navigator.pushNamed(context, "/home-page");
            }),
            CustomCardDrawer.getTheCard(context, Icons.hotel_rounded,
                "Mes Hotels", "cliquez pour gerer vos hotels", () {
              print("j'ai clique hotel ");
              Navigator.of(context).push(routeHotel);
            }),
            CustomCardDrawer.getTheCard(context, Icons.restaurant_rounded,
                "Mes Restaurants", "cliquez pour gerer vos restaurants", () {
              print("j'ai clique restaurant ");
            }),
            CustomCardDrawer.getTheCard(context, Icons.car_rental,
                "Mes Transports", "cliquez pour gerer vos transports", () {
              print("j'ai clique transport ");
            }),
            CustomCardDrawer.getTheCard(
                context,
                Icons.add_reaction_rounded,
                "Mes Espaces loisirs",
                "cliquez pour gerer vos espaces de loisirs", () {
              print("j'ai clique loisirs");
            }),
            CustomCardDrawer.getTheCard(
                context,
                Icons.public,
                "Mes Lieux de tourisme",
                "cliquez pour gerer vos espaces de tourisme", () {
              print("j'ai clique tourisme ");
            }),
          ],
        ),
      ),*/
      /* appBar: AppBar(
        backgroundColor: Colors.redAccent,
        title: Text('Accueil'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.logout_outlined,
            ),
            hoverColor: Colors.amber,
            tooltip: 'Se deconnecter',
            onPressed: () {
              UserService.logout().then((value) {
                widget.login.call();
                print("test logout");
                print(value);
                //Navigator.pushNamed(context, '/home-page');
              });
            },
          ),
        ],
      ), */
      //drawerEnableOpenDragGesture: true,
      floatingActionButton: FloatingActionButton(
        splashColor: Colors.redAccent[100],
        child: Icon(
          Icons.add,
          color: Colors.white,
          size: 40,
        ),
        backgroundColor: Colors.redAccent,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25))),
        tooltip: "Ajouter un nouvel hebergement",
        onPressed: (() {
          print("J'ajoute un nouvel hotel");
        }),
      ),

      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text(
              "Mes hebergements",
              style: TextStyle(
                color: Colors.redAccent.withOpacity(0.9),
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            centerTitle: true,
            pinned: true,
            floating: false,
            expandedHeight: mq.size.height / 5,
            backgroundColor: Colors.redAccent.withOpacity(0.9),
            foregroundColor: Colors.redAccent.withOpacity(0.9),
            flexibleSpace: Carousel(
              images: [
                AssetImage("assets/images/c1.jpg"),
                AssetImage("assets/voitures/v1.jpg"),
                AssetImage("assets/restaurants/r1.jpg"),
                AssetImage("assets/images/c5.jpg"),
                AssetImage("assets/images/c2.jpg"),
                AssetImage("assets/voitures/v2.jpg"),
                AssetImage("assets/restaurants/r2.jpg"),
                AssetImage("assets/images/c9.jpg"),
                AssetImage("assets/images/c3.jpg"),
                AssetImage("assets/voitures/v3.jpg"),
                AssetImage("assets/restaurants/r5.jpg"),
                AssetImage("assets/images/c7.jpg"),
                AssetImage("assets/images/c4.jpg"),
                AssetImage("assets/voitures/v4.jpg"),
                AssetImage("assets/restaurants/r4.jpg"),
                AssetImage("assets/images/c8.jpg"),
              ],
              dotSize: 8.0,
              dotSpacing: 15.0,
              dotColor: Colors.red,
              indicatorBgPadding: 4.0,
              dotBgColor: Colors.redAccent.withOpacity(0.3),
              dotIncreasedColor: Colors.redAccent.withOpacity(0.9),
              //dotPosition: DotPosition.topCenter,
              //borderRadius: true,
            ),
            //),
            /*actions: <Widget>[
              IconButton(
                icon: const Icon(
                  Icons.logout_outlined,
                ),
                hoverColor: Colors.amber,
                tooltip: 'Se deconnecter',
                onPressed: () {
                  UserService.logout().then((value) {
                    widget.login.call();
                    print("test logout1");
                    print(value);
                    Navigator.pushNamed(context, '/home-page');
                  });
                },
              ),
            ],*/
          ),
          SliverFixedExtentList(
            itemExtent: mq.size.height - 100,
            delegate: SliverChildListDelegate(
              [
                Container(
                  //height: MediaQuery.of(context).size.height * 4,
                  child: Form(
                      child: ListView(
                    physics: BouncingScrollPhysics(),
                    shrinkWrap: true,
                    children: [
                      SizedBox(
                        height: 70,
                        child: Padding(
                          padding: EdgeInsets.all(7),
                          child: typeLogement.getDropDownListItem(),
                        ),
                      ),
                      SizedBox(
                        height: 110,
                        child: Padding(
                          padding: EdgeInsets.all(7),
                          child: description.textfrofield(),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.03,
                        height: MediaQuery.of(context).size.height / 8,
                        margin: EdgeInsets.all(8),
                        /*decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                                strokeAlign: StrokeAlign.center,
                                color: Colors.black45, // Set border color
                                width: 1.0), // Set border width
                            borderRadius: BorderRadius.all(Radius.circular(
                                12.0)), // Set rounded corner radius
                            /*boxShadow: [
                                BoxShadow(
                                    blurRadius: 10,
                                    color: Colors.black,
                                    offset: Offset(1, 3))
                              ]*/ // Make rounded corner of border
                          ),*/
                        child: InputDecorator(
                          decoration: CustomInputDecoration(
                                  colorText: Colors.redAccent,
                                  hintText: "Localisation",
                                  labelText: "Localisation")
                              .getCustomInputDecoration(),
                          child: ListView(
                            physics: BouncingScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            children: [
                              Container(
                                  //height: MediaQuery.of(context).size.height / 1000,
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: pays.getDropDownListItem(),
                                  )),
                              Container(
                                  //height: MediaQuery.of(context).size.height / 1000,
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: ddVille,
                                  )),
                              Container(
                                  //height: MediaQuery.of(context).size.height / 1000,
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: ddQuartier,
                                  )),
                            ],
                          ),
                        ),
                      ),
                      Container(
                          width: MediaQuery.of(context).size.width / 1.03,
                          margin: EdgeInsets.all(7),
                          //padding: EdgeInsets.all(7),
                          child: InputDecorator(
                            decoration: CustomInputDecoration(
                                    colorText: Colors.redAccent,
                                    hintText: "Tarification",
                                    labelText: "Tarification")
                                .getCustomInputDecoration(),
                            child: Column(
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 1.03,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Container(
                                        padding: EdgeInsets.all(7),
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2,
                                        child: montantTarif.textfrofield(),
                                      ),
                                      Container(
                                        padding: EdgeInsets.all(7),
                                        height:
                                            MediaQuery.of(context).size.height /
                                                9.5,
                                        width:
                                            MediaQuery.of(context).size.width /
                                                2.5,
                                        child: dropDownValabite
                                            .getDropDownListItem(),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 1.04,
                                  child: descriptionTarif.textfrofield(),
                                ),
                              ],
                            ),
                          )),
                      Container(
                        width: MediaQuery.of(context).size.width / 1.03,
                        height: _listImagePaths.length > 0
                            ? MediaQuery.of(context).size.height / 3.28
                            : MediaQuery.of(context).size.height / 6,
                        padding: EdgeInsets.all(7),
                        child: InputDecorator(
                          decoration: CustomInputDecoration(
                                  colorText: Colors.redAccent,
                                  hintText: "Entrez les images du lieu",
                                  labelText: "Les images du lieu")
                              .getCustomInputDecoration(),
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Expanded(
                                    flex: 1,
                                    child: IconButton(
                                      alignment: Alignment(0, 0),
                                      padding: EdgeInsets.all(8),
                                      highlightColor:
                                          Colors.red.withOpacity(0.4),
                                      icon: Icon(
                                        Icons.photo_camera_outlined,
                                        color: Colors.redAccent,
                                        semanticLabel: "Galerie",
                                        size: 35,
                                      ),
                                      onPressed: () {
                                        selectImages();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                              _listImagePaths.length > 0
                                  ? Container(
                                      width: MediaQuery.of(context).size.width /
                                          1.03,
                                      height:
                                          MediaQuery.of(context).size.height /
                                              5,
                                      padding: EdgeInsets.all(5),
                                      child: InputDecorator(
                                        decoration: CustomInputDecoration(
                                                colorText: Colors.redAccent,
                                                hintText: "Vos choix",
                                                labelText: "Vos choix")
                                            .getCustomInputDecoration(),
                                        child: ListView.builder(
                                            physics: BouncingScrollPhysics(),
                                            scrollDirection: Axis.horizontal,
                                            itemCount: _listImagePaths.length,
                                            //shrinkWrap: true,
                                            /*gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              crossAxisCount: 2,
                                              childAspectRatio: 1.0,
                                              crossAxisSpacing: 10,
                                              mainAxisSpacing: 20),*/
                                            itemBuilder: (BuildContext context,
                                                int index) {
                                              return GestureDetector(
                                                onTap: () {
                                                  ImagePickers
                                                      .previewImagesByMedia(
                                                          _listImagePaths,
                                                          index);
                                                },
                                                child: Padding(
                                                  padding: EdgeInsets.all(8),
                                                  child: Container(
                                                    //height: 50,
                                                    decoration: BoxDecoration(
                                                      color: Colors.redAccent,
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5),
                                                    ),
                                                    //width: 100,
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              4.0),
                                                      child: Image.file(
                                                        width: 100,
                                                        height: 50,
                                                        File(_listImagePaths[
                                                                index]
                                                            .path!),
                                                        fit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              );
                                            }),
                                      ),
                                    )
                                  : SizedBox(
                                      //width: 10,  
                                      height: 10,
                                      child: Text(" "),
                                    ),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          color: Colors.redAccent,
                          border: Border.all(
                            width: 8,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                        //width: 10,
                        height: 100,
                        child: Text(" ja suis la"),
                      ),
                    ],
                  )),
                ),
                //cfh.getCustomFormHostel(context),
              ],
            ),
          )
        ],
      ),
    );
  }
}
