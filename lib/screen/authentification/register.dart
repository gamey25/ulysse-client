import 'package:flutter/material.dart';
import 'package:ulysse_all_front/model/non_register_user_model.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/service/user_service.dart';
import 'package:ulysse_all_front/widgets/customCheckboxListTile.dart';
import 'package:ulysse_all_front/widgets/customSnackBar.dart';
import 'package:ulysse_all_front/widgets/customTextField.dart';
import 'package:ulysse_all_front/widgets/custom_loading.dart';

class Register extends StatefulWidget {
  //const Login({super.key});
  final Function visible;
  Register(this.visible, {super.key});
  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  CustomTextField firstNameText = CustomTextField(
      title: "Nom", placeholder: "Enter first name", err: "Enter first name");

  CustomTextField secondNameText = CustomTextField(
      title: "Prenom",
      placeholder: "Enter second name",
      err: "Enter second name");

  CustomTextField phoneNumberText = CustomTextField(
      title: "phone number",
      placeholder: "Enter phone number",
      err: "Enter phone number");

  CustomTextField emailText = CustomTextField(
      title: "Email", placeholder: "Enter email", err: "Enter email");

  CustomTextField passText = CustomTextField(
      title: "Password",
      placeholder: "********",
      ispass: true,
      err: "Enter password");

  CustomTextField ConfirmPassText = CustomTextField(
      title: "Confirm password",
      placeholder: "********",
      ispass: true,
      err: "Enter password confirmation");

  CustomCheckBoxListTile proprioCheckbox = CustomCheckBoxListTile(
      title: "Proprietaire", subTitle: "Gestionnaire", status: true);
  CustomCheckBoxListTile clientCheckbox = CustomCheckBoxListTile(
      title: "Client", subTitle: "Utilisateur", status: false);

  bool bal = true;
  void changeProprio(e) {
    setState(() {
      proprioCheckbox.value = e;
      clientCheckbox.value = !e;
    });
  }

  void changeClient(e) {
    setState(() {
      proprioCheckbox.value = !e;
      clientCheckbox.value = e;
    });
  }

  String getThePosteName(bool valPro, bool valCli) {
    if (valPro == true) {
      return "Proprietaire";
    } else {
      return "Client";
    }
  }

  bool _loading = false;
  void activeLoadingTrue() {
    setState(() {
      _loading = true;
    });
  }

  void activeLoadingFalse() {
    setState(() {
      _loading = false;
    });
  }

  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    MediaQueryData queryData = MediaQuery.of(context);
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            //height: queryData.size.height,
            //width: queryData.size.width,
            padding: EdgeInsets.all(30),
            child: Form(
              key: _key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("Register",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.redAccent)),
                  SizedBox(
                    height: 10,
                  ),
                  firstNameText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  secondNameText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(width: 1, color: Colors.black38),
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Expanded(
                            child: proprioCheckbox
                                .checkBoxFormField(changeProprio)),
                        Expanded(
                            child:
                                clientCheckbox.checkBoxFormField(changeClient)),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  phoneNumberText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  emailText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  passText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  ConfirmPassText.textfrofield(),
                  SizedBox(
                    height: 5,
                  ),
                  _loading
                      ? CustomLoading(height: 30, width: 30, size: 30)
                          .customFieldLoading()
                      : SizedBox(
                          height: 1,
                        ),
                  ElevatedButton(
                    onPressed: () {
                      FormState? a = _key.currentState;
                      if (a != null && a.validate()) {
                        if (passText.value == ConfirmPassText.value) {
                          activeLoadingTrue();
                          NonRegisterUserModel nonRegisterUserModel =
                              NonRegisterUserModel(
                                  -1,
                                  firstNameText.value,
                                  secondNameText.value,
                                  phoneNumberText.value,
                                  getThePosteName(proprioCheckbox.value,
                                      clientCheckbox.value),
                                  emailText.value,
                                  passText.value);
                          Map<String, dynamic> data;
                          UserService.register(nonRegisterUserModel)
                              .then((value) {
                            data = value;
                            if (data['success'] == false) {
                              activeLoadingFalse();
                              Map<String, dynamic> message = data['message'];
                              print(data);
                              if (message.containsKey('phone_number')) {
                                CustomSnack.showSnackBar(context,
                                    "Le numero: ${phoneNumberText.value} est deja lie a un compte");
                              }
                              if (message.containsKey('email')) {
                                CustomSnack.showSnackBar(context,
                                    "Le mail: ${emailText.value} est deja lie a un compte");
                              }
                            } else {
                              activeLoadingFalse();
                              final user = data['user'];
                              print(user);
                              RegisterUserModel registerUserModel =
                                  RegisterUserModel(
                                user['id'],
                                user['first_name'],
                                user['second_name'],
                                user['phone_number'],
                                user['poste'],
                                user['email'],
                              );
                              registerUserModel
                                  .saveTokenAfterRegistering(data)
                                  .then((value) {
                                String returnMsg = value;
                                print(returnMsg);
                              });
                            }
                          });
                        } else {
                          CustomSnack.showSnackBar(context,
                              "Les maux de passe doivent etre identiques");
                        }
                      }
                    },
                    child: Text(
                      "Register",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          return Colors.redAccent.withOpacity(.7);
                        },
                      ),
                      foregroundColor:
                          MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          return Colors.black;
                        },
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Avez vous un compte?"),
                      TextButton(
                          onPressed: () {
                            widget.visible.call();
                          },
                          child: Text(
                            "Login",
                            style: TextStyle(color: Colors.redAccent),
                          ))
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
