import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/screen/boss_screen/accueil.dart';
import 'package:ulysse_all_front/service/user_service.dart';
import 'package:ulysse_all_front/widgets/customSnackBar.dart';
import 'package:ulysse_all_front/widgets/customTextField.dart';
import 'package:http/http.dart' as http;
import 'package:ulysse_all_front/widgets/custom_loading.dart';
import 'package:ulysse_all_front/widgets/loading.dart';

class Login extends StatefulWidget {
  //const Login({super.key});
  final Function visible;
  final Function(String nameZone) cliBoss;
  Login(this.visible, this.cliBoss, {super.key});
  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  CustomTextField emailText = new CustomTextField(
      title: "Email", placeholder: "Enter email", err: "Enter email");

  CustomTextField passText = new CustomTextField(
      title: "Password",
      placeholder: "********",
      ispass: true,
      err: "Enter password");

  void login(String email, String password) async {
    var url = Uri.http("172.29.160.1:80", "/api/all-user");
    final response = await http.get(url);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      print(response.body);
    }
  }

  bool _loading = false;
  void activeLoadingTrue() {
    setState(() {
      _loading = true;
    });
  }

  void activeLoadingFalse() {
    setState(() {
      _loading = false;
    });
  }

  final _key = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30),
            child: Form(
              key: _key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text("Login",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                          color: Colors.redAccent)),
                  SizedBox(
                    height: 30,
                  ),
                  emailText.textfrofield(),
                  SizedBox(
                    height: 10,
                  ),
                  passText.textfrofield(),
                  SizedBox(
                    height: 10,
                  ),
                  _loading
                      ? CustomLoading(height: 30, width: 30, size: 30)
                          .customFieldLoading()
                      : SizedBox(
                          height: 1,
                        ),
                  ElevatedButton(
                    onPressed: () {
                      FormState? a = _key.currentState;
                      if (a != null && a.validate()) {
                        activeLoadingTrue();
                        UserService.login(emailText.value, passText.value)
                            .then((result) {
                          activeLoadingFalse();
                          if (result['success'] == true) {
                            RegisterUserModel.login(result).then((val) {
                              print(result);
                              CustomSnack.showSnackBar(
                                  context, "Connexion reussie");
                              if (result["user"]["poste"] == "Proprietaire") {
                                widget.cliBoss.call("accueuil-boss");
                                //Navigator.pushNamed(context, '/accueil-boss');
                                //Navigator.pushNamed(context, '/accueil-client');
                              } else {
                                //Navigator.pushNamed(context, '/accueil-boss');
                                Navigator.pushNamed(context, '/accueil-client');
                              }
                            });
                          } else {
                            //activeLoadingFalse();
                            print(result);
                            if (result.containsKey('message_validator')) {
                              Map<String, dynamic> message_validator =
                                  result['message_validator'];
                              if (message_validator.containsKey('email')) {
                                CustomSnack.showSnackBar(
                                    context, "L'email est incorrect");
                              }
                              if (message_validator.containsKey('password')) {
                                CustomSnack.showSnackBar(
                                    context, "Mot de passe incorrect");
                              }
                            }
                            if (result.containsKey('message_simple')) {
                              CustomSnack.showSnackBar(
                                  context, result['message_simple']);
                            }
                          }
                        });
                      }

                      // FormState? a = _key.currentState;
                      // if (a != null && a.validate()) {
                      //   print(emailText.value);
                      // }
                    },
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          return Colors.redAccent.withOpacity(.7);
                        },
                      ),
                      foregroundColor:
                          MaterialStateProperty.resolveWith<Color?>(
                        (Set<MaterialState> states) {
                          return Colors.black;
                        },
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Avez vous un compte?"),
                      TextButton(
                          onPressed: () {
                            widget.visible.call();
                          },
                          child: Text(
                            "Register",
                            style: TextStyle(color: Colors.redAccent),
                          ))
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
