import 'package:flutter/material.dart';

class AccueilClient extends StatefulWidget {
  const AccueilClient({super.key});

  @override
  State<AccueilClient> createState() => _AccueilClientState();
}

class _AccueilClientState extends State<AccueilClient> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.redAccent,
        title: Text('Accueil Client'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.logout_outlined,
            ),
            tooltip: 'Se deconnecter',
            onPressed: () {
              // handle the press
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Text(
          "accueil client",
          style: TextStyle(color: Colors.redAccent),
        ),
      ),
    );
  }
}
