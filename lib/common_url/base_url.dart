import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class BaseUrl {
  static String serverAddress = "172.20.32.1:80";
  BaseUrl();

  dynamic getToken() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    final token = localStorage.getString('token');
    return jsonDecode(token!);
  }

  static Future<Map<String, String>> getHeader() async {
    SharedPreferences localStorage = await SharedPreferences.getInstance();
    final token = localStorage.getString('token');
    final jToken = jsonDecode(token!);
    print(jToken);
    return {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $jToken'
    };
  }
}
