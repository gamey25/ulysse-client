//import 'dart:js';

//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:ulysse_all_front/widgets/customTextField.dart';

class CustomFormHostel {
  //final BuildContext context;

  CustomTextField description = CustomTextField(
      title: "Description de l'hebergement",
      placeholder: "Enter une description",
      numLine: 4,
      err: "Enter une description");
  CustomTextField phoneNumberText = CustomTextField(
      title: "phone number",
      placeholder: "Enter phone number",
      numLine: 2,
      err: "Enter phone number");

  String selectedTypeHotel = "Chambre moderne";
  String selectedErrorTypeHotel = "Veuillez selectionner le type de logement";
  List<String> typeHotel = [
    "Chambre simple",
    "Chambre moderne",
    "Appartement simple",
    "Appartement meuble",
    "Studio simple",
    "Studio moderne",
    "Studio meuble"
  ];

  String selectedCountry = "Cameroun";
  String selectedErrorCountry = "Veuillez selectionner le pays";
  List<String> listCountry = [
    "Cameroun",
    "Cote d'ivoire",
    "Nigeria",
  ];

  String selectedCity = "Yaounde";
  String selectedErrorCity = "Veuillez selectionner la ville";
  List<String> listCity = [];
  List<String> listCity1 = [
    "Yaounde",
    "Douala",
    "Bafoussam",
  ];
  List<String> listCity2 = [
    "P2C1",
    "P2C2",
    "P2C3",
    "P2C4",
    "P2C5",
  ];
  List<String> listCity3 = [
    "P3C1",
    "P3C2",
  ];
  List<DropdownMenuItem<String>> listItemTypeCity = [];

  String selectedQuartier = "Mboppi";
  String selectedErrorQuartier = "Veuillez selectionner le quartier";
  List<String> listQuartier = [];
  List<String> listQuartier1 = [
    "Mboppi",
    "Mvog-ada",
    "Effoulan",
  ];
  List<String> listQuartier2 = [
    "C1Q15",
    "C2Q15",
    "C3Q15",
    "C4Q15",
    "C5Q15",
  ];
  List<String> listQuartier3 = [
    "C3Q1",
    "C3Q2",
  ];
  List<DropdownMenuItem<String>> listItemTypeQuartier = [];

  CustomFormHostel() {
    //selectedTypeHotel = typeHotel.elementAt(0);
    //listCity = listCity1;
    //listQuartier = listQuartier1;
    //getListItemCity();
    //getListItemQuartier();
  }

  List<DropdownMenuItem<String>> getListItemTypeHotel(List<String> typeHotel) {
    List<DropdownMenuItem<String>> listItemTypeHotel = [];
    typeHotel.forEach((element) {
      listItemTypeHotel.add(DropdownMenuItem(
        child: Text(
          element,
          style: TextStyle(color: Colors.redAccent, fontSize: 15),
        ),
        value: element,
        onTap: () {
          print("j'ai selectionne un nouveau type: $element");
        },
      ));
    });
    return listItemTypeHotel;
  }

  Widget getDropDownListTypeHotel(List<String> typeHotel) {
    return DropdownButtonFormField(
      items: this.getListItemTypeHotel(typeHotel),
      decoration: InputDecoration(
          hintText: "Type d'hebergement",
          labelText: "Type d'hebergement",
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
      hint: Text(
        "Selectionner le type d'hebergement",
        style: TextStyle(color: Colors.redAccent, fontSize: 15),
      ),
      elevation: 2,
      value: selectedTypeHotel,
      dropdownColor: Colors.redAccent[100],
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.selectedErrorTypeHotel;
        } else {
          return null;
        }
      },
      onChanged: (value) {
        selectedTypeHotel = value!;
        print("le nouveau type valide est: $value");
      },
    );
  }

  List<DropdownMenuItem<String>> getListItemCountry() {
    List<DropdownMenuItem<String>> listItemTypeHotel = [];
    listCountry.forEach((element) {
      listItemTypeHotel.add(DropdownMenuItem(
        child: Text(
          element,
          style: TextStyle(color: Colors.black, fontSize: 15),
        ),
        value: element,
        onTap: () {
          print("j'ai selectionne un nouveau Pays: $element");
        },
      ));
    });
    return listItemTypeHotel;
  }

  Widget getDropDownListCountry() {
    return DropdownButtonFormField(
      //itemHeight: 250,
      items: this.getListItemCountry(),

      decoration: InputDecoration(
          hintText: "Pays",
          labelText: "Pays",
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
      hint: Text(
        "Le Pays",
        style: TextStyle(color: Colors.redAccent, fontSize: 15),
      ),
      elevation: 2,
      dropdownColor: Colors.redAccent[100],
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.selectedErrorCountry;
        } else {
          return null;
        }
      },
      onChanged: (value) {
        selectedCountry = value!;
        if (selectedCountry.compareTo("Cameroun") == 0) {
          listCity = listCity1;
          //getListItemCity();
          //getListItemQuartier();
        }
        if (selectedCountry.compareTo("Cote d'ivoire") == 0) {
          listCity = listCity2;
          //getListItemCity();
          //getListItemQuartier();
        }
        if (selectedCountry.compareTo("Nigeria") == 0) {
          listCity = listCity3;

          //getListItemQuartier();
        }
        getListItemCity();
        print("le nouveau pays valide est: $value");
      },
    );
  }

  void getListItemCity() {
    //List<DropdownMenuItem<String>> listItemTypeCity2 = [];
    selectedCity = listCity.elementAt(0);
    //listItemTypeCity.clear();
    listCity.forEach((element) {
      listItemTypeCity.add(DropdownMenuItem(
        child: Text(
          element,
          style: TextStyle(color: Colors.black, fontSize: 15),
        ),
        value: element,
        onTap: () {
          print("j'ai selectionne une nouvelle ville: $element");
        },
      ));
    });
   
    //return listItemTypeCity;
  }

  Widget getDropDownListCity() {
    return DropdownButtonFormField(
      //itemHeight: 250,
      items: this.listItemTypeCity,

      decoration: InputDecoration(
          hintText: "Ville",
          labelText: "Ville",
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
      hint: Text(
        "La ville",
        style: TextStyle(color: Colors.redAccent, fontSize: 15),
      ),
      elevation: 2,
      value: selectedCity,
      dropdownColor: Colors.redAccent[100],
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.selectedErrorCity;
        } else {
          return null;
        }
      },
      onChanged: (value) {
        selectedCity = value!;
        if (selectedCity.compareTo("Yaounde") == 0) {
          listQuartier = listQuartier1;
          //getListItemQuartier();
        }
        if (selectedCity.compareTo("Douala") == 0) {
          listQuartier = listQuartier2;
          //getListItemQuartier();
        }
        if (selectedCity.compareTo("P2C5") == 0) {
          listQuartier = listQuartier3;
        }
        getListItemQuartier();
        print("la nouvelle ville valide est: $value");
      },
    );
  }

  void getListItemQuartier() {
    List<DropdownMenuItem<String>> listItemTypeQuartier2 = [];
    selectedQuartier = listQuartier.elementAt(0);
    //listItemTypeQuartier.clear();
    listQuartier.forEach((element) {
      listItemTypeQuartier2.add(DropdownMenuItem(
        child: Text(
          element,
          style: TextStyle(color: Colors.black, fontSize: 15),
        ),
        value: element,
        onTap: () {
          print("j'ai selectionne un nouveau Quartier: $element");
        },
      ));
    });
    listItemTypeQuartier = listItemTypeQuartier2;
    //return listItemTypeCity;
  }

  Widget getDropDownListQuartier() {
    return DropdownButtonFormField(
      //itemHeight: 250,
      items: this.listItemTypeQuartier,

      decoration: InputDecoration(
          hintText: "Quartier",
          labelText: "Quartier",
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
      hint: Text(
        "Le quartier",
        style: TextStyle(color: Colors.redAccent, fontSize: 15),
      ),
      elevation: 2,
      value: selectedQuartier,
      dropdownColor: Colors.redAccent[100],
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.selectedErrorQuartier;
        } else {
          return null;
        }
      },
      onChanged: (value) {
        selectedQuartier = value!;
        print("le nouveau quartier valide est: $value");
      },
    );
  }

  Widget getCustomFormHostel(BuildContext context) {
    return Container(
        //height: MediaQuery.of(context).size.height,
        //width: MediaQuery.of(context).size.width,
        decoration: ShapeDecoration(
          color: Colors.white,
          shape: Border.all(
            color: Colors.blue,
            width: 1.0,
          ),
        ),
        //color: Colors.redAccent,
        margin: EdgeInsets.all(1),
        padding: EdgeInsets.all(5),
        child: Form(
          child: Column(children: [
            /*SizedBox(
              height: 70,
              child: Padding(
                padding: EdgeInsets.all(5),
                child: this.getDropDownListTypeHotel(),
              ),
            ),
            SizedBox(
              height: 130,
              child: Container(
                  padding: EdgeInsets.all(5),
                  child: description.textfrofield()),
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 1.09,
              height: MediaQuery.of(context).size.height / 9,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: [
                  Container(
                      //height: MediaQuery.of(context).size.height / 1000,
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: this.getDropDownListCountry(),
                      )),
                  Container(
                      //height: MediaQuery.of(context).size.height / 1000,
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: this.getDropDownListCity(),
                      )),
                  Container(
                      //height: MediaQuery.of(context).size.height / 1000,
                      width: MediaQuery.of(context).size.width / 2.5,
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: this.getDropDownListQuartier(),
                      )),
                ],
              ),
            ),*/
          ]),
        ));
  }
}
