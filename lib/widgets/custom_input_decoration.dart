import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';

class CustomInputDecoration {
  final String hintText;
  final String labelText;
  final Color colorText;

  CustomInputDecoration(
      {this.hintText = "",
      this.labelText = "",
      this.colorText = Colors.redAccent});

  InputDecoration getCustomInputDecoration() {
    return InputDecoration(
        floatingLabelAlignment: FloatingLabelAlignment.center,
        focusColor: this.colorText,
        contentPadding: EdgeInsets.all(4),
        hintText: this.hintText,
        labelText: this.labelText,
        labelStyle: TextStyle(color: this.colorText),
        focusedBorder:
            OutlineInputBorder(borderSide: BorderSide(color: this.colorText)),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(12)),
        ));
  }
}
