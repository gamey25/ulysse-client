import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';
import 'package:ulysse_all_front/widgets/customSnackBar.dart';

class CustomCardDrawer {
  CustomCardDrawer();

  static Widget getTheCard(BuildContext context, IconData icon, String title,
      String subTitle, void Function() onClick ) {
    return Card(
      child: ListTile(
        hoverColor: Colors.amber,
        focusColor: Colors.amber,
        textColor: Colors.redAccent,
        leading: Icon(
          icon,
          color: Colors.redAccent[100],
          size: 40,
        ),
        title: Text(title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            )),
        subtitle: Text(
          subTitle,
          style: TextStyle(fontSize: 12),
        ),
        //trailing: Icon(Icons.more_vert),
        onTap: onClick,
      ),
    );
  }
}
