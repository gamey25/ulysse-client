import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class CustomSnack{

  
  CustomSnack();

  
  static void showSnackBar(BuildContext context, String msg1){
    final custonSnackBar = new SnackBar(content: Text(msg1));
    ScaffoldMessenger.of(context).showSnackBar(custonSnackBar);
  }
}