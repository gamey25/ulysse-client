import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CustomLoading {
  final double height;
  final double width;
  final double size;
  CustomLoading({this.size = 50, this.height = 50, this.width = 50});

  Widget customFieldLoading() {
    return Container(
      height: height,
      width: width,
      color: Colors.white,
      child: SpinKitDancingSquare(
          color: Colors.redAccent, size: size, duration: Duration(seconds: 3)),
    );
  }
}
