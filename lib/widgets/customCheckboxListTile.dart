import 'package:flutter/material.dart';

class CustomCheckBoxListTile {
  final String title;
  final String subTitle;
  bool status;
  //final void Function(bool?)? changeState;
  bool _value = false;
  CustomCheckBoxListTile(
      {this.title = "", this.subTitle = "", this.status = false}) {
    _value = status;
  }

  CheckboxListTile checkBoxFormField(void Function(bool?) change) {
    return CheckboxListTile(
        title: Text(title),
        value: _value,
        activeColor: Colors.redAccent,
        subtitle: Text(subTitle),
        controlAffinity: ListTileControlAffinity.leading,
        onChanged: change);
  }

  bool get value {
    return _value;
  }

  set value(bool val) {
    _value = val;
  }
}
