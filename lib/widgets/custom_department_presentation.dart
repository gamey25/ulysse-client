import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomDepartmentPresentation {
  CustomDepartmentPresentation();
  //int numStar = 4;

  static Widget getTheCustomDepartmentPresentation(
      {int numStar = 5,
      int numLog = 13,
      String intPrice = "12000",
      int numVille = 3,
      int numComment = 5800,
      String zone = "de logements",
      String urlImage = "assets/images/c1.jpg",
      //required BuildContext context,
      required void Function() onClick }) {
    return Padding(
      padding: EdgeInsets.all(1),
      child: Card(
          color: Colors.redAccent.withOpacity(0.8),
          child: Row(
            children: [
              Column(
                children: [
                  CircleAvatar(
                    backgroundColor: Colors.redAccent,
                    foregroundColor: Colors.white,
                    backgroundImage: AssetImage(urlImage),
                    radius: 25,
                    child: Text(
                      "L",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
                    ),
                  ),
                  SizedBox(
                    width: 100,
                    height: 40,
                    child: Row(
                      children: [
                        numStar >= 1
                            ? Icon(
                                Icons.star_border_purple500,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                        numStar >= 2
                            ? Icon(
                                Icons.star_border_purple500,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                        numStar >= 3
                            ? Icon(
                                Icons.star_border_purple500,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                        numStar >= 4
                            ? Icon(
                                Icons.star_border_purple500,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                        numStar >= 5
                            ? Icon(
                                Icons.star_border_purple500,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox(
                                width: 1,
                              ),
                      ],
                    ),
                  ),
                  SizedBox(
                    width: 90,
                    height: 30,
                    child: ElevatedButton(
                      child: Text(
                        "Plus... ",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15),
                      ),
                      style: ButtonStyle(
                          backgroundColor: MaterialStateProperty.all(
                            Colors.white,
                          ),
                          foregroundColor: MaterialStateProperty.all(
                            Colors.black,
                          ),
                          overlayColor: MaterialStateProperty.all(
                            Colors.redAccent,
                          ),
                          shape: MaterialStatePropertyAll(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                          )),
                      onPressed: onClick,
                    ),
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Text("Nombre $zone: ",
                        style: TextStyle(
                          color: Colors.black,
                        )),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Text("Prix max (XAF): ",
                        style: TextStyle(
                          color: Colors.black,
                        )),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Text("Nombre de villes: ",
                        style: TextStyle(
                          color: Colors.black,
                        )),
                  ),
                  Padding(
                    padding: EdgeInsets.all(8),
                    child: Text("Nombre de commentaires: ",
                        style: TextStyle(
                          color: Colors.black,
                        )),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(4),
                    decoration: ShapeDecoration(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: Text(
                      "${numLog}",
                      style: TextStyle(
                        color: Colors.black,
                        //backgroundColor: Colors.redAccent[100]
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(4),
                    decoration: ShapeDecoration(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: Text(
                      "${intPrice}",
                      style: TextStyle(
                        color: Colors.black,
                        //backgroundColor: Colors.redAccent[100]
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(4),
                    decoration: ShapeDecoration(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: Text(
                      "${numVille}",
                      style: TextStyle(
                        color: Colors.black,
                        //backgroundColor: Colors.redAccent[100]
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.all(4),
                    padding: EdgeInsets.all(4),
                    decoration: ShapeDecoration(
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    child: Text(
                      "${numComment}",
                      style: TextStyle(
                        color: Colors.black,
                        //backgroundColor: Colors.redAccent[100]
                      ),
                    ),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
