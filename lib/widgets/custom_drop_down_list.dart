import 'package:flutter/material.dart';

class CustomDropDownList {
  List<String> listItem;
  String label = "le label";
  String err = "Veuillez selectionner l'error";
  String hintText = "Selectionner le hint text";
  String val = "";
  //List<DropdownMenuItem<String>> listItemTypeHotel = [];
  void Function(String? changeValue) onChange;
  CustomDropDownList(
      {required this.listItem,
      required this.onChange,
      this.label = "",
      this.err = "",
      this.hintText = ""}) {
    this.val = this.listItem.elementAt(0);
  }

  List<DropdownMenuItem<String>> getListMenuItem() {
    List<DropdownMenuItem<String>> listItemTypeHotel = [];
    //listItemTypeHotel.clear();
    listItem.forEach((element) {
      //print("j'ai selectionne un nouvel element: $element");
      listItemTypeHotel.add(DropdownMenuItem(
        child: Text(
          element,
          style: TextStyle(color: Colors.redAccent, fontSize: 15),
        ),
        value: element,
        onTap: () {
          print("j'ai selectionne un nouvel element: $element");
        },
      ));
    });
    return listItemTypeHotel;
  }

  Widget getDropDownListItem() {
    //getListMenuItem();
    return DropdownButtonFormField(
      items: getListMenuItem(),
      decoration: InputDecoration(
          hintText: this.hintText,
          labelText: this.label,
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
      hint: Text(
        this.hintText,
        style: TextStyle(color: Colors.redAccent, fontSize: 15),
      ),
      elevation: 2,
      value: val,
      dropdownColor: Colors.white,
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.err;
        } else {
          return null;
        }
      },
      onChanged: onChange,
    );
  }
}
