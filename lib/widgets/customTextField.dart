import 'package:flutter/material.dart';

class CustomTextField {
  final String title;
  final String placeholder;
  final bool ispass;
  final int numLine;
  String err;
  TextEditingController controller = TextEditingController();
  String _value = "";
  CustomTextField(
      {this.title = "",
      this.placeholder = "",
      this.ispass = false,
      this.numLine = 1,
      this.err = "veuillez specifier ce champ"});
  TextFormField textfrofield() {
    return TextFormField(
      onChanged: ((e) {
        _value = e;
      }),
      validator: (e) {
        if (e != null && e.isEmpty) {
          return this.err;
        } else {
          return null;
        }
      },
      controller: controller,
      obscureText: this.ispass,
      minLines: this.numLine,
      maxLines: this.ispass == true? 1:this.numLine+1,
      decoration: InputDecoration(
          hintText: this.placeholder,
          labelText: this.title,
          labelStyle: TextStyle(color: Colors.redAccent),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.redAccent)),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(12)),
          )),
    );
  }

  String get value {
    return _value;
  }
}
