import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:ulysse_all_front/common_url/base_url.dart';
import 'package:ulysse_all_front/common_url/user_endpoint.dart';
import 'package:ulysse_all_front/model/non_register_user_model.dart';
import 'package:ulysse_all_front/model/register_user_model.dart';

class UserService {
  UserService();
  static Future<Map<String, dynamic>> register(
      NonRegisterUserModel nonRegisteruser) async {
    var url = Uri.http(BaseUrl.serverAddress, UserEndPoint.registerEndPoint);
    final response = await http.post(url, body: nonRegisteruser.toMap());
    //print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  static Future<Map<String, dynamic>> login(
      String email, String password) async {
    var url = Uri.http(BaseUrl.serverAddress, UserEndPoint.loginEndPoint);
    final response =
        await http.post(url, body: {"email": email, "password": password});
    print(jsonDecode(response.body));
    return jsonDecode(response.body);
  }

  static Future<bool> logout() async {
    bool logoutFront = false;
    Map<String, String>? header;
    BaseUrl.getHeader().then((value) async {
      header = value;
      print("check header befire send");
      print(value);
      var url = Uri.http(BaseUrl.serverAddress, UserEndPoint.logoutEndPoint);
      final response = await http.get(url, headers: header);
      print(response.body);
      Map<String, dynamic> jRes = json.decode(response.body);

      if (jRes.containsKey("success")) {
        if (jRes["success"] == true) {
          RegisterUserModel.logOutUser().then((valLogout) {
            print("la value logout: $valLogout");
            if (valLogout == true) {
              logoutFront = true;
            } else {
              logoutFront = false;
            }
          });
        } else {
          logoutFront = false;
        }
      } else {
        logoutFront = false;
      }
      print("je suis la");
      print(header);
    });

    return logoutFront;
  }
}
